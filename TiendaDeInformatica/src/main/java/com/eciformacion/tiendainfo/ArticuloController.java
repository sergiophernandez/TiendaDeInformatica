package com.eciformacion.tiendainfo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eciformacion.tiendainfo.persistencia.GestorArticulos;
import com.eciformacion.tiendainfo.persistencia.pojos.Articulo;

@RestController
@RequestMapping("/apiarticulos")
public class ArticuloController {
	
	@Autowired
	private GestorArticulos gestorArticulos;
	
	public GestorArticulos getGestorArticulos() {
		return gestorArticulos;
	}

	public void setGestorArticulos(GestorArticulos gestorArticulos) {
		this.gestorArticulos = gestorArticulos;
	}
	
	@GetMapping("/articulos")
	public List getAllArticulos() {
		return (List)gestorArticulos.getArticulomapper().getArticulos();		
	}
	
	@GetMapping("/articulos/{id}")
	public Articulo getUnArticulo(@PathVariable("id") String id) {
		
		return gestorArticulos.getArticulomapper().getArticulo(Integer.valueOf(id));
	}

}
