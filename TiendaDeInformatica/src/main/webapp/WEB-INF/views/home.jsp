<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<html>
<head>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css"
	integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb"
	crossorigin="anonymous">
<title>Home</title>
<script type="text/javascript">
	function show(bloq) {
		obj = document.getElementById(bloq);
		obj.style.display = (obj.style.display == 'none') ? 'block' : 'none';
	}
</script>
</head>
<body>
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
		integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"
		integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh"
		crossorigin="anonymous"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"
		integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ"
		crossorigin="anonymous"></script>

	<div class="container">

		<c:forEach items="${fabricantes}" var="fabricante">
			<div class="d-inline bg-success">
				<p class="font-weight-bold h1">${fabricante.getNombre_fabricante()}
					<a href="deleteFabricante/${fabricante.getCodigo_fabricante()}"><button
							type="submit" class="btn btn-danger">Eliminar</button></a>

					<button type="button" class="btn btn-primary" data-toggle="modal"
						data-target="#exampleModal" data-whatever="@fat">Modificar</button>
				<div class="modal fade" id="exampleModal" tabindex="-1"
					role="dialog" aria-labelledby="exampleModalLabel"
					aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">Modificar Fabricante</h5>
								<button type="button" class="close" data-dismiss="modal"
									aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<form action="modificarFabricante" method="POST">
									<div class="form-group">
										<label for="recipient-name" class="col-form-label">Nombre:</label>
										<input type="text" class="form-control" id="recipient-name"
											name="nombre_fabricante">
									</div>
									<div class="form-group">
										<label for="recipient-name" class="col-form-label">Apellidos:</label>
										<input type="text" class="form-control" id="recipient-name"
											name="apellidos_fabricante">
									</div>
									<div class="form-group">
										<label for="recipient-name" class="col-form-label">Tel�fono:</label>
										<input type="text" class="form-control" id="recipient-name"
											name="telefono_fabricante">
									</div>
									<input type="hidden" class="form-control" id="recipient-name"
										name="codigo_fabricante"
										value="${fabricante.getCodigo_fabricante()}">
										
									<div class="modal-footer">
										<button type="button" class="btn btn-secondary"
											data-dismiss="modal">Close</button>
										<button type="submit" class="btn btn-primary">Modificar</button>
									</div>
								</form>
							</div>

						</div>
					</div>
				</div>
				</p>

			</div>

			<div class="d-inline bg-success">
				<p class="font-weight-bold h2">
					Tabla de Articulos <a href="nuevoArticulo"><button
							type="submit" class="btn btn-success">Nuevo Art�culo</button></a>

				</p>
			</div>

			<table class="table">
				<thead>
					<tr>
						<th>C�digo</th>
						<th>Art�culo</th>
						<th>Precio</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${articulos}" var="articulo">
						<c:if
							test="${articulo.getFabricante() == fabricante.getCodigo_fabricante()}">
							<tr>
								<td><c:out value="${articulo.getCodigo_articulo()}"></c:out></td>
								<td><a href="articulo/${articulo.getIdprueba()}"><c:out
											value="${articulo.getNombre_articulo()}"></c:out></a></td>
								<td><c:out value="${articulo.getPrecio()}"></c:out></td>
								<td><a href="articuloborrar/${articulo.getIdprueba()}"><button
											type="submit" class="btn btn-danger">Eliminar</button></a></td>
								<td><a href="modifarticulo/${articulo.getIdprueba()}">
										<button type="submit" class="btn btn-primary">Modificar</button>
								</a></td>
								<td><button type="button" class="btn btn-success">A�adir
										al carro</button></td>
							</tr>
						</c:if>
					</c:forEach>
				</tbody>
			</table>
		</c:forEach>
	</div>

</body>
</html>
