package com.eciformacion.tiendainfo.persistencia.mappers;

import java.util.List;

import com.eciformacion.tiendainfo.persistencia.pojos.Articulo;

public interface ArticuloMapper {

	public List<Articulo> getArticulos();

	public Articulo getArticulo(int idprueba);

	public void deleteArticulo(int idprueba);
	
	public void modArticulo(Articulo art);
	
	public void nuevoArticulo(Articulo art);
	
}
