package com.eciformacion.tiendainfo.persistencia.pojos;

import java.io.Serializable;
import java.math.BigDecimal;

public class Articulo implements Serializable {
	
	private String codigo_articulo, nombre_articulo;
	private Double precio;
	private int fabricante, idprueba;
	
	public Articulo() {
		super();
	}
	public Articulo(String codigo_articulo, String nombre_articulo, Double precio, int fabricante, int idprueba) {
		super();
		this.codigo_articulo = codigo_articulo;
		this.nombre_articulo = nombre_articulo;
		this.precio = precio;
		this.fabricante = fabricante;
		this.idprueba = idprueba;
	}
	public String getCodigo_articulo() {
		return codigo_articulo;
	}
	public void setCodigo_articulo(String codigo_articulo) {
		this.codigo_articulo = codigo_articulo;
	}
	public String getNombre_articulo() {
		return nombre_articulo;
	}
	public void setNombre_articulo(String nombre_articulo) {
		this.nombre_articulo = nombre_articulo;
	}
	public double getPrecio() {
		return precio;
	}
	public void setPrecio(double precio) {
		this.precio = precio;
	}
	public int getFabricante() {
		return fabricante;
	}
	public void setFabricante(int fabricante) {
		this.fabricante = fabricante;
	}
	public int getIdprueba() {
		return idprueba;
	}
	public void setIdprueba(int idprueba) {
		this.idprueba = idprueba;
	}

}
