package com.eciformacion.tiendainfo.persistencia;

import com.eciformacion.tiendainfo.persistencia.mappers.FabricanteMapper;

public class GestorFabricantes {
	private FabricanteMapper fabricanteMapper;

	public FabricanteMapper getFabricanteMapper() {
		return fabricanteMapper;
	}

	public void setFabricanteMapper(FabricanteMapper fabricanteMapper) {
		this.fabricanteMapper = fabricanteMapper;
	}
	
	
}
