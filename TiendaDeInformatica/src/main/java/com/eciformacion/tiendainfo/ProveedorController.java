package com.eciformacion.tiendainfo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eciformacion.tiendainfo.persistencia.GestorFabricantes;
import com.eciformacion.tiendainfo.persistencia.pojos.Fabricante;

@RestController
@RequestMapping("/apifabricantes")
public class ProveedorController {

	@Autowired
	private GestorFabricantes gestorFabricantes;
	
	public GestorFabricantes getGestorArticulos() {
		return gestorFabricantes;
	}

	public void GestorFabricantes(GestorFabricantes gestorFabricantes) {
		this.gestorFabricantes = gestorFabricantes;
	}
	
	@GetMapping("/fabricantes")
	public List getAllProveedores() {
		return (List)gestorFabricantes.getFabricanteMapper().getFabricantes();	
	}
	
	@GetMapping("/fabricantes/{id}")
	public Fabricante getUnProveedor(@PathVariable("id") String id) {
		return gestorFabricantes.getFabricanteMapper().getFabricante(Integer.valueOf(id));	
	}
	
}
