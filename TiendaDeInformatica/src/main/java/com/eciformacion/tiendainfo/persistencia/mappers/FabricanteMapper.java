package com.eciformacion.tiendainfo.persistencia.mappers;

import java.util.List;

import com.eciformacion.tiendainfo.persistencia.pojos.Articulo;
import com.eciformacion.tiendainfo.persistencia.pojos.Fabricante;

public interface FabricanteMapper {
	public List<Fabricante> getFabricantes();
	public Fabricante getFabricante(int codigo_fabricante);
	public int deleteFabricante(int codigo_fabricante);
	public void altaFabricante(Fabricante fabricante);
	public void updateFabricante(Fabricante fabricante);
	public List<Articulo> getArticuloFrabicante(int fabricante);
}
