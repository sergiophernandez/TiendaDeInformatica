package com.eciformacion.tiendainfo;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.eciformacion.tiendainfo.persistencia.GestorArticulos;
import com.eciformacion.tiendainfo.persistencia.GestorFabricantes;
import com.eciformacion.tiendainfo.persistencia.pojos.Articulo;
import com.eciformacion.tiendainfo.persistencia.pojos.Fabricante;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Handles requests for the application home page.
 */

@Controller
public class HomeController {
	ObjectMapper mapper = new ObjectMapper();
	@Autowired
	private GestorFabricantes gestorFabricantes;
	
	@ModelAttribute("articulo")
	public Articulo createArticulo(@RequestParam(name = "codigo", defaultValue = "000000") String codigo, 
			@RequestParam(name = "nombre", defaultValue = "0") String nombre, 
			@RequestParam(name = "precio", defaultValue = "0.0") String precio, 
			@RequestParam(name = "fabricante", defaultValue = "0") String fabricante, 
			@RequestParam(name = "idprueba", defaultValue = "0") String idprueba) {
		
		return new Articulo(codigo, nombre, Double.valueOf(precio), Integer.valueOf(fabricante), Integer.valueOf(idprueba));
	}

	@Autowired
	private GestorArticulos gestorArticulos;
	

	public GestorArticulos getGestorArticulos() {
		return gestorArticulos;
	}


	public void setGestorArticulos(GestorArticulos gestorArticulos) {
		this.gestorArticulos = gestorArticulos;
	}


	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		model.addAttribute("articulos", gestorArticulos.getArticulomapper().getArticulos());
		model.addAttribute("fabricantes", this.gestorFabricantes.getFabricanteMapper().getFabricantes());
		return "home";
	}
		
	@GetMapping("/articulo/{id}")
	public String getAlumno(@PathVariable("id") int id, Model model) {
		model.addAttribute("articulo", gestorArticulos.getArticulomapper().getArticulo(id));
		return "detalle";
	}
	
	
	@GetMapping("/articuloborrar/{idprueba}")
	public String deleteDetalle(@PathVariable("idprueba") int idprueba, Model model) {
		gestorArticulos.getArticulomapper().deleteArticulo(idprueba);
		return "redirect:/";
	}
	
	
	@GetMapping("/modifarticulo/{idprueba}")
	public String modifArticulo(@PathVariable("idprueba") int idprueba, Model model) {	
		model.addAttribute("artmod", gestorArticulos.getArticulomapper().getArticulo(idprueba));
		return "modifarticulo";
	}
	
	@PostMapping("/modArticulo")
	public String modArticulo(@ModelAttribute("articulo") Articulo art, Model model) {
		gestorArticulos.getArticulomapper().modArticulo(art);
		return "redirect:/";
	}
	
	@GetMapping("/nuevoArticulo")
	public String nuevoArticulo(Model model) {
		return "nuevoarticulo";
	}
	
	@PostMapping("/nuevoArticulo")
	public String newArticulo(@ModelAttribute("articulo") Articulo art, Model model) {
		gestorArticulos.getArticulomapper().nuevoArticulo(art);
		return "redirect:/";
	}

	@GetMapping("/fabricante/{codigo_fabricante}")
	public String getFabricante(@PathVariable("codigo_fabricante") int codigo_fabricante, Model model) {
		model.addAttribute("fabricanteId", this.gestorFabricantes.getFabricanteMapper().getFabricante(codigo_fabricante));
		return "home";
	}
	
	@GetMapping("/deleteFabricante/{fabricante}")
	public String deleteFabricante(@PathVariable("fabricante") int fabricante, Model model) {
		this.gestorFabricantes.getFabricanteMapper().deleteFabricante(fabricante);
		return "redirect:/";
	}
	
	@PostMapping("altaFabricante")
	public String altaFabricante(@RequestParam("nombre_fabricante") String nombre_fabricante,@RequestParam("apellidos_fabricante") String apellidos_fabricante,
			@RequestParam("telefono_fabricante") String telfFab, Model model) {
		Fabricante fabricante = new Fabricante();
		fabricante.setCodigo_fabricante(0);
		fabricante.setNombre_fabricante(nombre_fabricante);
		fabricante.setApellidos_fabricante(apellidos_fabricante);
		long telefono_fabricante = Long.parseLong(telfFab);
		fabricante.setTelefono_fabricante(telefono_fabricante);
		this.gestorFabricantes.getFabricanteMapper().altaFabricante(fabricante);
		return "redirect:/";
	}
	
	@PostMapping("/modificarFabricante")
	public String actualizarFabricante(@RequestParam("nombre_fabricante") String nombre_fabricante, @RequestParam("apellidos_fabricante") String apellidos_fabricante,
			@RequestParam("telefono_fabricante") String telefono_fabricante, @RequestParam("codigo_fabricante") String codigo_fabricante,Model model) {
		Fabricante fab = new Fabricante();
		fab.setCodigo_fabricante(Integer.parseInt(codigo_fabricante));
		fab.setNombre_fabricante(nombre_fabricante);
		fab.setApellidos_fabricante(apellidos_fabricante);
		fab.setTelefono_fabricante(Long.parseLong(telefono_fabricante));
		this.gestorFabricantes.getFabricanteMapper().updateFabricante(fab);
		return "redirect:/";
	}
	
//	@GetMapping("/articulo/{fabricante}")
//	public String listarArticulos(@PathVariable("fabricante") int fabricante, Model model) {
//		model.addAttribute("listaArticulo", this.gestorFabricantes.getFabricanteMapper().getArticuloFrabicante(fabricante));
//		return "home";
//	}
	public GestorFabricantes getGestorFabricantes() {
		return gestorFabricantes;
	}

	public void setGestorFabricantes(GestorFabricantes gestorFabricantes) {
		this.gestorFabricantes = gestorFabricantes;
	}
	
}
