package com.eciformacion.tiendainfo.persistencia;

import org.springframework.beans.factory.annotation.Autowired;

import com.eciformacion.tiendainfo.persistencia.mappers.ArticuloMapper;

public class GestorArticulos {

	@Autowired
	private ArticuloMapper articuloMapper;

	public ArticuloMapper getArticulomapper() {
		return articuloMapper;
	}

	public void setArticuloMapper(ArticuloMapper articuloMapper) {
		this.articuloMapper = articuloMapper;
	}
	
}
